[Dokumentasjon ved bruk av AI](./02-DOKUMENTASJON-AI.md) &bullet; [README](../README.md) &bullet; [Oppgave 1: flervalg](./04-OPG1-FLERVALG.md)
# Generell informasjon
Denne eksamen har tre seksjoner:

1. Flervalg (oppgave 1, tilsammen 10 poeng)
2. Forklaring (oppgave 2, tilsammen 15 poeng)
3. Koding (oppgave 3 og 4, tilsammen 40 poeng)


Oppgave 2, 3 og 4 refererer til kode du kan laste ned fra https://git.app.uib.no/ii/inf101/23v/exam/inf101v23exam. I oppgave 3 og 4 skal du skrive koden din i en kopi av dette repositoriet. Begynn med å fork'e først og deretter klone ned prosjektet, eller eventuelt last ned repoet som en zip-fil. Sjekk gjerne at du kan kjøre `MainColorWhack` i pakken *no.uib.inf101.exam23v.colorwhack* lokalt på din egen maskin; hvis du får problemer med dette, har vi to tekniske vakter tilgjengelig som kan hjelpe deg.


Oppgave 1-2 skal besvares direkte på vurdering.uib.no. Innlevering av oppgave 3 og 4 skjer i form av en .zip -fil du laster opp på punkt 5 på vurdering.uib.no. Hvis noe skulle gå skrekkelig galt med datamaskinen din underveis, er det selvfølgelig veldig lurt om du da har jobbet med din egen fork av prosjektet og pushet jevnlig underveis, da vil du ikke tape alt arbeidet ditt.
