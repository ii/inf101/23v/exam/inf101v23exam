[Oppgave 1: flervalg](./04-OPG1-FLERVALG.md) &bullet; [README](../README.md) &bullet; [Oppgave 3: Whack-a-mole misclick](./06-OPG3-WHACK-A-MOLE.md)

# Oppgave 2: Tetromino-fabrikken

> Viktig: denne oppgaven skal besvares i Inspera (vurdering.uib.no)

Denne oppgaven omhandler semesteroppgave 1 (Tetris). I det utleverte repositoriet finner du et utdrag av en mulig løsning på oppgaven i pakken [no.uib.inf101.exam23v.tetromino](../src/main/java/no/uib/inf101/exam23v/tetromino/) (der ligger en delvis fullført [Tetromino.java](../src/main/java/no/uib/inf101/exam23v/tetromino/Tetromino.java)).


Fasongen til en Tetromino er her representert av en to-dimensjonal boolsk array.

```java
/**
 * A tetromino is a shape that can be used in the game of Tetris. It 
 * consists of four quadratic blocks glued together along the edges.
 * <p>
 *   
 * This class is immutable.
 */
public final class Tetromino implements Iterable<GridCell<Character>> {

  private final char symbol;
  private final boolean[][] shape; //   <-------
  private final CellPosition pos;
  /* ... */
}
```

I test-hierarkiet finner du klassen [PatternedTetrominoFactory](../src/test/java/no/uib/inf101/exam23v/tetromino/PatternedTetrominoFactory.java), som kan lage nye brikker etter et bestemt mønster. En medstudent i kurset ønsker å teste at denne tetrominofabrikken fungerer. I [TestPatternedTetrominoFactory](../src/test/java/no/uib/inf101/exam23v/tetromino/TestPatternedTetrominoFactory.java) har hen derfor en metode som skal teste et veldig enkelt mønster, nemlig tilfellet at vi kun skal få T-brikker fra fabrikken: 

```java
@Test
public void testFactoryProducingOnlyT() {
  PatternedTetrominoFactory factory = new PatternedTetrominoFactory("T");
  
  for (int i = 0; i < NUMBER_OF_TRAILS; i++) {
    Tetromino tetro = factory.getNext();
    boolean[][] shapeTetro = null;  // TODO: get the shape of the tetromino here
    
    // Expected shape of T-piece:
    // 0 0 0
    // 1 1 1
    // 0 1 0
    assertFalse(shapeTetro[0][0]);
    assertFalse(shapeTetro[0][1]);
    assertFalse(shapeTetro[0][2]);
    
    assertTrue(shapeTetro[1][0]);
    assertTrue(shapeTetro[1][1]);
    assertTrue(shapeTetro[1][2]);
    
    assertFalse(shapeTetro[2][0]);
    assertTrue(shapeTetro[2][1]);
    assertFalse(shapeTetro[2][2]);  
  }
}
```

Studenten har nå kommet til deg for å be om hjelp, og påstår (feilaktig?) at testen nesten er ferdig, «det mangler jo bare en linje».

Forklar (med ord, ikke med lange kodeavsnitt) til studenten hvordan du kan gjøre testen ferdig. Hvis det er flere måter å gjøre det på (hint, hint!), må du forklare fordeler og ulemper med hensyn til god stil med objektorientert programmering.

Vi anbefaler at du skriver minst 2-3 avsnitt, men maksimalt 400 ord
