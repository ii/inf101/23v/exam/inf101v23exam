package no.uib.inf101.exam23v.colorwhack.view;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * A view for the buttons in the ColorWhack game.
 * This view contains a reset button.
 */
public class ButtonsView extends JPanel {
  
  private final JButton reset;

  /** Creates a new buttons view. */
  public ButtonsView() {
    this.reset = new JButton("Reset");
    this.add(this.reset);
  }

  /** Gets the reset button. */
  public JButton getResetButton() {
    return this.reset;
  }
  
}
