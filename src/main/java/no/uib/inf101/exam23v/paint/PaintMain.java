package no.uib.inf101.exam23v.paint;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

public class PaintMain {
  public static void main(String[] args) {

    JPanel view = new JPanel(); // TODO: replace with your own stuff

    JFrame frame = new JFrame();
    frame.setMinimumSize(new Dimension(200, 90));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101 Paint");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
